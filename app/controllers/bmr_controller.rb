class BmrController < ApplicationController
  def index
  end
  def create
  	weight=params[:user][:weight].to_f
  	height=params[:user][:height].to_f
  	age=params[:user][:age].to_f
  	if params[:user][:gender]=="Male"
  		@bmr = 88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age)
  	else
  		@bmr = 447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age)
  	end
  	@bmr=@bmr.round(3)
  	user=User.new
  	user.first_name=params[:user][:first_name]
  	user.last_name=params[:user][:last_name]
  	user.weight=weight
  	user.height=height
  	user.age=age
  	if params[:user][:gender]=="Male"
  		user.gender = true	
  	else
  		user.gender = false
  	end	
  	user.bmr=@bmr
  	user.save!
  	render :show
  end
  def show
  	
  end
end
