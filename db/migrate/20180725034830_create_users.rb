class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
   	  t.string :first_name
      t.string :last_name
      t.integer :weight
      t.integer :height
      t.integer :age
      t.boolean :gender

      t.timestamps null: false
    end
  end
end
